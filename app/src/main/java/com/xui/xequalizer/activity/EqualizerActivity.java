package com.xui.xequalizer.activity;


import android.content.Context;
import android.logic.XuiContext;
import android.logic.XuiManager;
import android.os.IBinder;
import android.os.IEqCallback;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.adapter.MyFragmentPagerAdapter;
import com.xui.xequalizer.fragment.EffectFragment;
import com.xui.xequalizer.fragment.FieldFragment;
import com.xui.xequalizer.view.MyFragementViewPager;

import java.util.ArrayList;
import java.util.List;


public class EqualizerActivity extends FragmentActivity implements
        RadioGroup.OnCheckedChangeListener, ViewPager.OnPageChangeListener {

    private static final String TAG = "EqualizerActivity";

    private MyFragementViewPager mViewPager;
    //  顶部Button
    private RadioGroup mRadioGroup;
    private RadioButton mRbEffect, mRbField;

    private View mSwitchIndicator;  //  顶部SwitchButton选中背景

    private List<Fragment> mAlFragments;

    private XuiManager mXuiManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initXuiService();

        setContentView(R.layout.activity_main);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        initView();
        initData();
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {

    }

    private void initView() {
        mRadioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        mRbEffect = (RadioButton) findViewById(R.id.rb_effect);
        mRbField = (RadioButton) findViewById(R.id.rb_field);
        mViewPager = (MyFragementViewPager) findViewById(R.id.viewPager);
        mSwitchIndicator = findViewById(R.id.switch_selected_bg);
    }


    private void initData() {
        //  音效和声场
        EffectFragment mEffectFragment = new EffectFragment();
        FieldFragment mFieldFragment = new FieldFragment();

        mAlFragments = new ArrayList<>();
        mAlFragments.add(mEffectFragment);
        mAlFragments.add(mFieldFragment);

        mRadioGroup.setOnCheckedChangeListener(this);
        mViewPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), mAlFragments));
        mViewPager.addOnPageChangeListener(this);

        mRadioGroup.check(R.id.rb_effect);
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rb_effect:
                mViewPager.setIsScroll(true);
                mViewPager.setCurrentItem(0);
                mViewPager.setIsScroll(false);
                break;
            case R.id.rb_field:
                mViewPager.setIsScroll(true);
                mViewPager.setCurrentItem(1);
                mViewPager.setIsScroll(false);
                break;
            default:
                break;
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) mSwitchIndicator.getLayoutParams();
        params.width = mRadioGroup.getMeasuredWidth() / 2 - 4;
        params.topMargin = 4;
        params.bottomMargin = 4;

        params.leftMargin = (int) (4 + (position * params.width + positionOffset * params.width));
        mSwitchIndicator.setLayoutParams(params);
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    /**
     * 开启Xui Framework服务
     */
    private void initXuiService() {

        try {
            mXuiManager = (XuiManager) getSystemService(XuiContext.XUI_GENERAL_SERVICE);
            mXuiManager.registerEqCallback(new IEqCallback.Stub() {
                @Override
                public void eqOnOffNotice(boolean b) throws RemoteException {
                    Log.d(TAG, "eqOnOffNotice : " + b);
                }

                @Override
                public void loudOnOffNotice(boolean b) throws RemoteException {
                    Log.d(TAG, "loudOnOffNotice : " + b);
                }

                @Override
                public void subWoofOnOffNotice(boolean b) throws RemoteException {
                    Log.d(TAG, "subWoofOnOffNotice : " + b);
                }

                @Override
                public void currSoundNotice(int[] ints) throws RemoteException {
                    Log.d(TAG, "currSoundNotice : " + ints);
                }

                @Override
                public void currSoundXYNotice(int[] ints) throws RemoteException {
                    Log.d(TAG, "currSoundXYNotice : " + "x = " + ints[0] + " y = " + ints[1]);
                }
            });
            XEqualizerApp.setXuiManager(mXuiManager);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            mXuiManager.unregisterEqCallback(new IEqCallback() {
                @Override
                public void eqOnOffNotice(boolean b) throws RemoteException {

                }

                @Override
                public void loudOnOffNotice(boolean b) throws RemoteException {

                }

                @Override
                public void subWoofOnOffNotice(boolean b) throws RemoteException {

                }

                @Override
                public void currSoundNotice(int[] ints) throws RemoteException {

                }

                @Override
                public void currSoundXYNotice(int[] ints) throws RemoteException {

                }

                @Override
                public IBinder asBinder() {
                    return null;
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }
}
