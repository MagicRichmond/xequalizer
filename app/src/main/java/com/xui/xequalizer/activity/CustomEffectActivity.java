package com.xui.xequalizer.activity;

import android.logic.XuiManager;
import android.os.RemoteException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;


public class CustomEffectActivity extends AppCompatActivity {

    private RelativeLayout mRelativeLayout; //  返回键

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_effect);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        mRelativeLayout = (RelativeLayout) findViewById(R.id.btn_effect_back);
        mRelativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

    }
}
