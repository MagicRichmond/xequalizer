package com.xui.xequalizer;

import android.app.Application;
import android.content.Context;
import android.logic.XuiManager;


/**
 * Created by gcf on 2016/10/8.
 */

public class XEqualizerApp extends Application {

    private static Context sContext;

    private static XuiManager mXuiManager;


    @Override
    public void onCreate() {
        sContext = getApplicationContext();
    }

    public static Context getContext() {
        return sContext;
    }

    public static void setXuiManager(XuiManager xuiManager) {
        mXuiManager = xuiManager;
    }

    public static XuiManager getXuiManager() {
        return mXuiManager;
    }
}
