package com.xui.xequalizer.view;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * VP禁止左右滑动
 * Created by gcf on 2016/9/30.
 */

public class MyFragementViewPager extends ViewPager {

    private boolean isScroll = false;

    public MyFragementViewPager(Context context) {
        super(context);
    }

    public MyFragementViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (!isScroll) {
            return false;
        } else {
            return super.onTouchEvent(ev);
        }
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (!isScroll) {
            return false;
        } else {
            return super.onInterceptTouchEvent(ev);
        }

    }

    public void setIsScroll(boolean isScroll) {
        this.isScroll = isScroll;
    }
}
