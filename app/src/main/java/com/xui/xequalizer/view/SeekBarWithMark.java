package com.xui.xequalizer.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.logic.XuiManager;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.utils.SharePreUtils;


/**
 * 自定义进度条
 */
public class SeekBarWithMark extends LinearLayout {

    private static final String TAG = "SeekBarWithMark";

    private XuiManager mXuiManager;

    private Context mContext;

    public SeekBar mSeekBar;
    private static int[] curSoundInfo;

    //根据 mMarkItemNum 来选择，整除(mMarkItemNum - 1)最好
    private int MAX = 50;
    //刻度的数目，默认9
    private int mMarkItemNum = 15;
    //相邻刻度的间距
    private int mSpacing = MAX / (mMarkItemNum - 1);
    //当前选中的数
    private int nowMarkItem;

    private OnSelectItemListener mSelectItemListener;

    //SeekBar的ProgressDrawable和Thumb
    private Drawable mProgressDrawable, mThumbDrawable;
    //刻度的文字大小
    private float mMarkTextSize;

    //  左侧音效模式，右侧刻度数
    private TextView mMarkTextView, mDirTextView;


    private SeekBarWithMark(Context context) {
        super(context);
    }

    public SeekBarWithMark(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBarWithMark(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mContext = XEqualizerApp.getContext();
        /**
         * 获取保存的自定义中高音
         */
        mXuiManager = XEqualizerApp.getXuiManager();

        mMarkTextView = new TextView(getContext());
        mDirTextView = new TextView(getContext());

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.SeekBarWithMark);

        mDirTextView.setText(a.getText(R.styleable.SeekBarWithMark_sbw_directionText));

        final int N = a.getIndexCount();
        for (int i = 0; i < N; i++) {
            int attr = a.getIndex(i);
            //
            if (attr == R.styleable.SeekBarWithMark_sbw_markItemNum) {
                mMarkItemNum = a.getInteger(attr, 31);
                MAX = (mMarkItemNum - 1) * 10;
                mSpacing = MAX / (mMarkItemNum - 1);
            } else if (attr == R.styleable.SeekBarWithMark_sbw_progressDrawabler) {
                mProgressDrawable = a.getDrawable(attr);
            } else if (attr == R.styleable.SeekBarWithMark_sbw_thumbDrawable) {
                Drawable drawable = a.getDrawable(attr);
                mThumbDrawable = drawable;
            } else if (attr == R.styleable.SeekBarWithMark_sbw_markTextSize) {
                mMarkTextSize = a.getDimensionPixelSize(attr, 32);
            }
        }
        a.recycle();
        init();
    }


    private void init() {
        this.setOrientation(HORIZONTAL);
        this.setGravity(Gravity.CENTER);

        try {
            curSoundInfo = mXuiManager.getSoundInfo();
            Log.d(TAG, "curSoundInfo[0] = " + curSoundInfo[0]);
            Log.d(TAG, "curSoundInfo[1] = " + curSoundInfo[1]);
            Log.d(TAG, "curSoundInfo[2] = " + curSoundInfo[2]);
        } catch (RemoteException e) {
            e.printStackTrace();
        }


        mSeekBar = (SeekBar) View.inflate(getContext(), R.layout.custom_seekbar, null);
        mSeekBar.setMax(MAX);
        //设置ProgressDrawable
        mSeekBar.setProgressDrawable(mProgressDrawable);
        //设置Thumb
        mSeekBar.setThumb(mThumbDrawable);
        // 设置Thumb阴影覆盖问题
        mSeekBar.setSplitTrack(false);

        LayoutParams mSeekBarLp = new LayoutParams(500, ViewGroup.LayoutParams.WRAP_CONTENT);
        mSeekBar.setLayoutParams(mSeekBarLp);


        if (getId() == R.id.treble_bar) {
            mMarkTextView.setText(curSoundInfo[2] + "");
            mSeekBar.setProgress((curSoundInfo[2] + 15) * 10);
        }
        if (getId() == R.id.alto_bar) {
            mSeekBar.setProgress((curSoundInfo[1] + 15) * 10);
            mMarkTextView.setText(curSoundInfo[1] + "");
        }
        if (getId() == R.id.bass_bar) {
            mSeekBar.setProgress((curSoundInfo[0] + 15) * 10);
            mMarkTextView.setText(curSoundInfo[0] + "");
        }


        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            private int shouldInProgress;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                nowMarkItem = Math.round(progress / mSpacing);
                shouldInProgress = mSpacing * nowMarkItem;
                SeekBarWithMark.this.mSeekBar.setProgress(shouldInProgress);
                //  调节范围从-15到15
                mMarkTextView.setText(shouldInProgress / 10 - 15 + "");

                //  判断滑动哪个进度条
                try {
                    if (getId() == R.id.treble_bar) {
                        curSoundInfo[2] = shouldInProgress / 10 - 15;
                        System.out.println("高音 ： " + curSoundInfo[2]);
                    }
                    if (getId() == R.id.alto_bar) {
                        curSoundInfo[1] = shouldInProgress / 10 - 15;
                        System.out.println("中音 ： " + curSoundInfo[1]);
                    }
                    if (getId() == R.id.bass_bar) {
                        curSoundInfo[0] = shouldInProgress / 10 - 15;
                        System.out.println("低音 ： " + curSoundInfo[0]);
                    }

                    Log.d(TAG, " 0 = " + curSoundInfo[0] + " 1 = " + curSoundInfo[1] + " 2 = " + curSoundInfo[2]);
                    mXuiManager.setSoundInfo(curSoundInfo[0], curSoundInfo[1], curSoundInfo[2]);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                if (mSelectItemListener != null) {
//                    mSelectItemListener.selectItem(nowMarkItem, mMarkDescArray[nowMarkItem]);

                }

                //  保存自定义三个值
                SharePreUtils.putUVolume("initUVol", curSoundInfo[0], mContext);
                SharePreUtils.putMVolume("initMVol", curSoundInfo[1], mContext);
                SharePreUtils.putHVolume("initHVol", curSoundInfo[2], mContext);
            }
        });

        //  左侧音效模式
        LayoutParams dirLayoutParams = new LayoutParams(100, 100);
        dirLayoutParams.rightMargin = 21;
        dirLayoutParams.gravity = Gravity.CENTER;
        mDirTextView.setTextSize(mMarkTextSize);
        mDirTextView.setTextColor(Color.WHITE);
        mDirTextView.setGravity(Gravity.CENTER);
        mDirTextView.setLayoutParams(dirLayoutParams);
        //  右侧刻度数
        LayoutParams markLayoutParams = new LayoutParams(100, 100);
        markLayoutParams.leftMargin = 30;
        markLayoutParams.gravity = Gravity.CENTER;
        mMarkTextView.setTextSize(mMarkTextSize);
        mMarkTextView.setTextColor(Color.WHITE);
        mMarkTextView.setGravity(Gravity.CENTER);
        mMarkTextView.setLayoutParams(markLayoutParams);

        this.addView(mDirTextView);
        this.addView(this.mSeekBar);
        this.addView(mMarkTextView);
    }


    /**
     * 选中监听
     */

    public interface OnSelectItemListener {
        void selectItem(int nowSelectItemNum, String val);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        mSeekBar.setEnabled(enabled);
    }


}
