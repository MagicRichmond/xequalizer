package com.xui.xequalizer.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.xui.xequalizer.R;


/**
 * Created by gcf on 2016/10/12.
 */

public class CustomCardView extends RelativeLayout {

    private Context mContext;

    public CustomCardView(Context context) {
        super(context);
        mContext = context;
        initView(context);
    }

    public CustomCardView(Context context, AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        initView(context);
    }

    public CustomCardView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        initView(context);
    }

    private void initView(Context context) {
        View customView = inflate(context, R.layout.fragment_effect_item, null);
        LayoutParams layoutParams = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        customView.setLayoutParams(layoutParams);
        addView(customView);
    }


}
