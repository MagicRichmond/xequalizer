package com.xui.xequalizer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.logic.XuiManager;
import android.os.IBinder;
import android.os.IEqCallback;
import android.os.RemoteException;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;


import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.activity.EqualizerActivity;
import com.xui.xequalizer.utils.SharePreUtils;


/**
 * 自定义声场调节
 * Created by gcf on 2016/9/30.
 */

public class CustomDotView extends View {

    private static final String TAG = "CustomDotView";

    private XuiManager mXuiManager = XEqualizerApp.getXuiManager();

    //  小球按下位置
    private float beginX;
    private float beginY;

    //  声场可调区域
    private static float startX, endX, startY, endY;

    //  小球初始化所在位置
    private float currentX = SharePreUtils.getDotX("dotBeginX", (endX - startX) / 2 + startX, XEqualizerApp.getContext());
    private float currentY = SharePreUtils.getDotY("dotBeginY", (endY - startY) / 2 + startY, XEqualizerApp.getContext());
    //  小球内外层, 外层为表格每格宽度， 内层为外层2/3
    private static int mInnerRadius, mOuterRadius;
    private Paint mInnerPaint = new Paint();
    private Paint mOuterPaint = new Paint();



    private static float perSpace;  //  表格每格宽度

    private boolean isInTouch;  //  判断按下位置是否在范围内，否则不允许滑动
    private boolean isScroll;   //  判断是否是滑动后抬起

    public CustomDotView(Context context) {
        super(context);
    }

    public CustomDotView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomDotView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public CustomDotView(Context context, int left, int top, int width, int height) {
        super(context);

        startX = left;
        startY = top;
        endX = startX + width;
        endY = startY + height;
        Log.d(TAG + " 表格相对坐标 ：", " x = " + startX + " y = " + startY + " width = " + endY);

        perSpace = (endX - startX) / 14;
        mOuterRadius = (int) perSpace;
        mInnerRadius = mOuterRadius / 3 * 2;
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        mInnerPaint.setColor(Color.WHITE);
        mOuterPaint.setColor(Color.WHITE);
        mOuterPaint.setAlpha(8);

        System.out.println("Init : x = " + currentX + "   y = " + currentY);
        canvas.drawCircle(currentX, currentY, mOuterRadius, mOuterPaint);
        canvas.drawCircle(currentX, currentY, mInnerRadius, mInnerPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                beginX = event.getX();  //  滑动起始位置
                beginY = event.getY();

                currentX = event.getX();
                currentY = event.getY();

                //  按下在范围内生效
                if (currentX > startX - mOuterRadius && currentX < endX + mOuterRadius
                        && currentY > startY - mOuterRadius && currentY < endY + mOuterRadius) {
                    adjustPosition();
                    postInvalidate();
                    isInTouch = true;
                } else {
                    isInTouch = false;
                }
                break;
            case MotionEvent.ACTION_MOVE:
                if (isInTouch && Math.abs(beginX - event.getX()) > 6  //  最小滑动距离
                        && Math.abs(beginY - event.getY()) > 6) {

                    currentX = event.getX();
                    currentY = event.getY();
                    //  禁止滑动超出范围
                    if (currentX < startX) {
                        currentX = startX;
                    } else if (currentX > endX) {
                        currentX = endX;
                    }
                    if (currentY < startY) {
                        currentY = startY;
                    } else if (currentY > endY) {
                        currentY = endY;
                    }

                    //  计算每个空格距离，四舍五入到就近点
                    int tempXNum = Math.round((currentX - startX) / perSpace);
                    currentX = tempXNum * perSpace + startX;
                    int tempYNum = Math.round((currentY - startY) / perSpace);
                    currentY = tempYNum * perSpace + startY;

                    //  发送x,y坐标（car坐标为实际坐标旋转270度）
                    try {
                        mXuiManager.setSoundXY(7 - tempYNum, 7 - tempXNum);
                    } catch (RemoteException e) {
                        e.printStackTrace();
                    }

                    postInvalidate();
                    isScroll = true;
                }
                break;
            case MotionEvent.ACTION_UP:
                if (isScroll && isInTouch) {
                    adjustPosition();
                } else {
                    currentX = SharePreUtils.getDotX("dotBeginX", (endX - startX) / 2 + startX, XEqualizerApp.getContext());
                    currentY = SharePreUtils.getDotY("dotBeginY", (endY - startY) / 2 + startY, XEqualizerApp.getContext());
                }
                isScroll = false;
                break;
        }
        return true;
    }


    /**
     * 抬起小球时调整到就近位置
     */
    private void adjustPosition() {
        if (currentX < startX || currentX > endX) {
            currentX = currentX < startX ? startX : currentX;
            currentX = currentX > endX ? endX : currentX;
            return;
        }
        if (currentY < startY || currentY > endY) {
            currentY = currentY < startY ? startY : currentY;
            currentY = currentY > endY ? endY : currentY;
            return;
        }

        //  计算每个空格距离，四舍五入到就近点
        float perSpace = (endX - startX) / 14;
        int tempXNum = Math.round((currentX - startX) / perSpace);
        currentX = tempXNum * perSpace + startX;
        int tempYNum = Math.round((currentY - startY) / perSpace);
        currentY = tempYNum * perSpace + startY;

        //  当前声场x，y坐标
        Log.d(TAG, "x = " + (7 - tempYNum));
        Log.d(TAG, "y = " + (7 - tempXNum));


        //  发送x,y坐标
        try {
            mXuiManager.setSoundXY(7 - tempYNum, 7 - tempXNum);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
}
