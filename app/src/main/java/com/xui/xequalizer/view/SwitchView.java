package com.xui.xequalizer.view;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.xui.xequalizer.R;

/**
 * Created by MaTeng on 16/11/9.
 */

public class SwitchView extends RelativeLayout {

    private static final int animDuration = 300;

    private ImageView progress;

    private ImageView thumb;

    //上次点击的时间
    private long lastClickTime;

    public SwitchView(Context context) {
        super(context);
    }

    public SwitchView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SwitchView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public SwitchView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        setBackgroundResource(R.drawable.switch_frame_selector);

        thumb = new ImageView(getContext());
        thumb.setImageResource(R.drawable.switch_slider_selector);
        LayoutParams params1 = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params1.addRule(RelativeLayout.CENTER_VERTICAL);
        params1.rightMargin = 4;
        params1.leftMargin = 4;
        thumb.setLayoutParams(params1);
        progress = new ImageView(getContext());
        progress.setImageResource(R.drawable.car_settings_btn_on_bg);
        progress.setScaleType(ImageView.ScaleType.MATRIX);
        LayoutParams params2 = new LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params2.addRule(RelativeLayout.CENTER_VERTICAL);

        if (isSelected()) {
            params2.width = 80;
        } else {
            params2.width = 0;

        }

        progress.setLayoutParams(params2);


        addView(progress);
        addView(thumb);


        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                switchAnimation();
            }
        });
        post(new Runnable() {
            @Override
            public void run() {
                if (isSelected()) {
                    thumb.setTranslationX(getMeasuredWidth() - thumb.getMeasuredWidth() - 4 - 4);
                    thumb.setSelected(true);
                }
            }


        });
    }

    public void initSwitchStatus(boolean switchStatus) {
        setSelected(switchStatus);
    }


    public void switchAnimation() {
        if (System.currentTimeMillis() - lastClickTime <= animDuration)
            return;
        float startValue = 0;
        float endValue = getMeasuredWidth() - thumb.getMeasuredWidth() - 4 - 4;
        final float value;
        if (isSelected()) {
            startValue = endValue;
            endValue = 0;
            value = startValue;
        } else {
            value = endValue;
        }

        ObjectAnimator oa = ObjectAnimator.ofFloat(thumb, "translationX", startValue, endValue);
        oa.setDuration(animDuration);
        oa.start();
        lastClickTime = System.currentTimeMillis();

        setSelected(!isSelected());

        oa.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                LayoutParams params = (LayoutParams) progress.getLayoutParams();
                params.width = (int) (thumb.getMeasuredWidth() / 2 + Float.parseFloat(animation.getAnimatedValue().toString()));
                progress.setLayoutParams(params);

                progress.setAlpha(Float.parseFloat(animation.getAnimatedValue().toString()) / value);

            }
        });

        oa.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });


    }

}
