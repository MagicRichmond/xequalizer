package com.xui.xequalizer.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gcf on 2016/10/8.
 */

public class SharePreUtils {

    /**
     * 保存声场初始位置
     * @param key
     * @param beginX
     * @param context
     */
    public static void putDotX(String key, float beginX, Context context) {
        SharedPreferences sp = context.getSharedPreferences("dotBeginX", Context.MODE_PRIVATE);
        sp.edit().putFloat(key, beginX).commit();
    }

    public static void putDotY(String key, float beginY, Context context) {
        SharedPreferences sp = context.getSharedPreferences("dotBeginY", Context.MODE_PRIVATE);
        sp.edit().putFloat(key, beginY).commit();
    }

    public static float getDotX(String key, float defBeginX, Context context) {
        SharedPreferences sp = context.getSharedPreferences("dotBeginX", Context.MODE_PRIVATE);
        return sp.getFloat(key, defBeginX);
    }

    public static float getDotY(String key, float defBeginY, Context context) {
        SharedPreferences sp = context.getSharedPreferences("dotBeginY", Context.MODE_PRIVATE);
        return sp.getFloat(key, defBeginY);
    }

    /**
     * 保存中高低音
     * @param key
     * @param initHVol
     * @param context
     */

    public static void putHVolume(String key, int initHVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initHVol", Context.MODE_PRIVATE);
        sp.edit().putInt(key, initHVol).commit();
    }

    public static void putMVolume(String key, int initMVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initMVol", Context.MODE_PRIVATE);
        sp.edit().putInt(key, initMVol).commit();
    }

    public static void putUVolume(String key, int initUVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initUVol", Context.MODE_PRIVATE);
        sp.edit().putInt(key, initUVol).commit();
    }

    public static int getHVolume(String key, int defHVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initHVol", Context.MODE_PRIVATE);
        return sp.getInt("initHVol", defHVol);
    }

    public static int getMVolume(String key, int defMVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initMVol", Context.MODE_PRIVATE);
        return sp.getInt("initMVol", defMVol);
    }

    public static int getUVolume(String key, int defUVol, Context context) {
        SharedPreferences sp = context.getSharedPreferences("initUVol", Context.MODE_PRIVATE);
        return sp.getInt("initUVol", defUVol);
    }


    /**
     * 重低音和等响度开关
     * @param key
     * @param isLoudOn
     * @param context
     */

    public static void putIsLoudOn(String key, boolean isLoudOn, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isLoudOn", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, isLoudOn).commit();
    }

    public static boolean getIsLoudOn(String key, boolean isLoudOn, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isLoudOn", Context.MODE_PRIVATE);
        return sp.getBoolean("isLoudOn", isLoudOn);
    }


    public static void putIsSubWoofOn(String key, boolean isSubWoofOn, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isSubWoofOn", Context.MODE_PRIVATE);
        sp.edit().putBoolean(key, isSubWoofOn).commit();
    }

    public static boolean getIsSubWoofOn(String key, boolean isSubWoofOn, Context context) {
        SharedPreferences sp = context.getSharedPreferences("isSubWoofOn", Context.MODE_PRIVATE);
        return sp.getBoolean("IsSubWoofOn", isSubWoofOn);
    }

}
