package com.xui.xequalizer.fragment;

import android.content.Intent;
import android.logic.XuiContext;
import android.logic.XuiManager;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.xui.xequalizer.view.twowaygallery.TwoWayAdapterView;
import com.xui.xequalizer.view.twowaygallery.flow.CoverFlow;
import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.activity.CustomEffectActivity;
import com.xui.xequalizer.adapter.EffectAdapter;
import com.xui.xequalizer.utils.SharePreUtils;

import java.util.Arrays;


/**
 * A simple {@link Fragment} subclass.
 */
public class EffectFragment extends BaseFragment implements
        TwoWayAdapterView.OnItemSelectedListener, TwoWayAdapterView.OnItemClickListener {

    private static final String TAG = "EffectFragment";

    private XuiManager mXuiManager;

    //  五种模式
    private int[] curSoundInfo = {0, 0, 0};
    private int[] jazzSoundInfo = {0, 3, 5};
    private int[] rockSoundInfo = {10, -2, 6};
    private int[] popSoundInfo = {-4, -1, -2};
    private int[] classicSoundInfo = {2, 0, 2};

    private View mView;

    private CoverFlow mCoverFlow;   //  画廊
    private EffectAdapter mEffectAdapter;

    private int selectedItem;   //  选中位置
    private int curSelectedItem;    // 当前选中模式

    //  选中时和未选中时的图片背景
    private int[] unselectedImages = new int[]{
            R.drawable.eq_icon_jazz,
            R.drawable.eq_icon_rock,
            R.drawable.eq_icon_manuall,
            R.drawable.eq_icon_pop,
            R.drawable.eq_icon_classic};

    private int[] selectedImages = new int[]{
            R.drawable.eq_icon_jazz_select,
            R.drawable.eq_icon_rock_select,
            R.drawable.eq_icon_manuall_select,
            R.drawable.eq_icon_pop_select,
            R.drawable.eq_icon_classic_select};

    public EffectFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_effect, container, false);

        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        mCoverFlow = (CoverFlow) mView.findViewById(R.id.mGallery);
        mEffectAdapter = new EffectAdapter(XEqualizerApp.getContext(), selectedImages, unselectedImages);
        mCoverFlow.setAdapter(mEffectAdapter);

        this.mXuiManager = (XuiManager) XEqualizerApp.getContext().getSystemService(XuiContext.XUI_GENERAL_SERVICE);
        setInitMode();
//        try {
//            mXuiManager.registerEqCallback(new IEqCallback.Stub() {
//                @Override
//                public void eqOnOffNotice(boolean b) throws RemoteException {
//
//                }
//
//                @Override
//                public void loudOnOffNotice(boolean b) throws RemoteException {
//
//                }
//
//                @Override
//                public void subWoofOnOffNotice(boolean b) throws RemoteException {
//
//                }
//
//                @Override
//                public void currSoundNotice(int[] ints) throws RemoteException {
//                    Log.d("get Notice = ", " 1 = " + ints[0] + " 2 = " + ints[1] + " 3 = " + ints[2]);
//                    System.arraycopy(ints, 0, curSoundInfo, 0, 3);
//                    judgeMode();
//                }
//
//                @Override
//                public void currSoundXYNotice(int[] ints) throws RemoteException {
//
//                }
//            });
//        } catch (RemoteException e) {
//            e.printStackTrace();
//        }





        mCoverFlow.setOnItemSelectedListener(this);
        mCoverFlow.setOnItemClickListener(this);

    }

    /**
     * 通过get得到数据设置初始模式
     */
    private void setInitMode() {
        try {
            System.arraycopy(this.mXuiManager.getSoundInfo(), 0, curSoundInfo, 0, 3);
            Log.d("get Info" , " 1 = " + mXuiManager.getSoundInfo()[0] + " 2 = " + mXuiManager.getSoundInfo()[1] + " 3 = " + mXuiManager.getSoundInfo()[2]);
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        judgeMode();
    }

    /**
     * 获取 当前mcu音效数组值， 并判断模式
     */
    private void judgeMode() {

        if (Arrays.equals(curSoundInfo, jazzSoundInfo)) {
            mCoverFlow.setSelection(0);
        } else if (Arrays.equals(curSoundInfo, rockSoundInfo)) {
            mCoverFlow.setSelection(1);
        } else if (Arrays.equals(curSoundInfo, popSoundInfo)) {
            mCoverFlow.setSelection(3);
        } else if (Arrays.equals(curSoundInfo, classicSoundInfo)) {
            mCoverFlow.setSelection(4);
        } else {
            mCoverFlow.setSelection(2);
            SharePreUtils.putUVolume("initUVol", curSoundInfo[0], XEqualizerApp.getContext());
            SharePreUtils.putMVolume("initMVol", curSoundInfo[1], XEqualizerApp.getContext());
            SharePreUtils.putHVolume("initHVol", curSoundInfo[2], XEqualizerApp.getContext());
        }
    }

    @Override
    public void onItemSelected(TwoWayAdapterView<?> parent, View view, int position, long id) {
        Log.d(TAG, " onItemSelected = " + position);
        mEffectAdapter.setSelectItem(position);
        this.selectedItem = position;
        this.curSelectedItem = position % selectedImages.length;

        this.setSoundInfo();
    }

    /**
     * 想mcu发送模式信息
     */
    private void setSoundInfo() {
        try {
            switch (curSelectedItem) {
                case 0:
                    mXuiManager.setSoundInfo(jazzSoundInfo[0], jazzSoundInfo[1], jazzSoundInfo[2]);
                    break;
                case 1:
                    mXuiManager.setSoundInfo(rockSoundInfo[0], rockSoundInfo[1], rockSoundInfo[2]);
                    break;
                case 2:
                    mXuiManager.setSoundInfo(curSoundInfo[0], curSoundInfo[1], curSoundInfo[2]);
                    break;
                case 3:
                    mXuiManager.setSoundInfo(popSoundInfo[0], popSoundInfo[1], popSoundInfo[2]);
                    break;
                case 4:
                    mXuiManager.setSoundInfo(classicSoundInfo[0], classicSoundInfo[1], classicSoundInfo[2]);
                    break;
                default:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onNothingSelected(TwoWayAdapterView<?> parent) {

    }

    @Override
    public void onItemClick(TwoWayAdapterView<?> parent, View view, int position, long id) {

        //  当前状态为选中，且是自定义模式（值为2）时跳到音效调节界面
        if (this.selectedItem == position && curSelectedItem == 2) {
            startActivity(new Intent(getActivity(), CustomEffectActivity.class));
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
