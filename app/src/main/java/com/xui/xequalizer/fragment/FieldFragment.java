package com.xui.xequalizer.fragment;

import android.logic.XuiManager;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IEqCallback;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;


import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.activity.EqualizerActivity;
import com.xui.xequalizer.utils.SharePreUtils;
import com.xui.xequalizer.view.CustomDotView;
import com.xui.xequalizer.view.SwitchView;

/**
 * A simple {@link Fragment} subclass.
 */
public class FieldFragment extends BaseFragment implements View.OnClickListener {

    private static final String TAG = "FieldFragment";

    private XuiManager mXuiManager;

    private View mView;
    //  重低音，等响度判断开关
    private boolean isLoudOn, isSubWoofOn;

    //  重低音，等响度自定义开关
    private SwitchView btnLoud;
    private SwitchView btnSubWoof;

    private ImageView mEpGrid;


    public FieldFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mXuiManager = XEqualizerApp.getXuiManager();
        try {
            isLoudOn = mXuiManager.getLoudOnOff();
            Log.d(TAG, "isLoudOn = " + isLoudOn);
            isSubWoofOn = mXuiManager.getSubWoofOnOff();
            Log.d(TAG, "isSubWoofOn = " + isSubWoofOn);


            mXuiManager.registerEqCallback(new IEqCallback.Stub() {
                @Override
                public void eqOnOffNotice(boolean b) throws RemoteException {

                }

                @Override
                public void loudOnOffNotice(boolean b) throws RemoteException {
                    Log.d(TAG, "isLoudOn = " + b);
                    btnLoud.setSelected(b);
                }

                @Override
                public void subWoofOnOffNotice(boolean b) throws RemoteException {
                    Log.d(TAG, "isSubWoofOn = " + b);
                    btnSubWoof.setSelected(b);
                }

                @Override
                public void currSoundNotice(int[] ints) throws RemoteException {

                }

                @Override
                public void currSoundXYNotice(int[] ints) throws RemoteException {
                    System.out.println("6666666666666 x = " + ints[0] + "    y = " + ints[1]);
                }
            });
        } catch (RemoteException e) {
            e.printStackTrace();
        }

        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_field, container, false);


        mEpGrid = (ImageView) mView.findViewById(R.id.eq_grid);

        btnLoud = (SwitchView) mView.findViewById(R.id.btn_loud);
        btnSubWoof = (SwitchView) mView.findViewById(R.id.btn_subWoof);

        btnLoud.initSwitchStatus(isLoudOn);
        btnSubWoof.initSwitchStatus(isSubWoofOn);

        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        btnLoud.setOnClickListener(this);
        btnSubWoof.setOnClickListener(this);


        mEpGrid.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                new CustomDotView(XEqualizerApp.getContext(), mEpGrid.getLeft(), mEpGrid.getTop(), mEpGrid.getWidth(), mEpGrid.getHeight());
                SharePreUtils.putDotX("dotBeginX", (mEpGrid.getLeft() + mEpGrid.getWidth() / 2), XEqualizerApp.getContext());
                SharePreUtils.putDotY("dotBeginY", (mEpGrid.getTop() + mEpGrid.getHeight() / 2), XEqualizerApp.getContext());
            }
        });

        try {
            SharePreUtils.putDotX("dotBeginX", mXuiManager.getCurrSoundXY()[0], XEqualizerApp.getContext());
            SharePreUtils.putDotY("dotBeginY", mXuiManager.getCurrSoundXY()[1], XEqualizerApp.getContext());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            //  等响度开关
            case R.id.btn_loud:
                try {
                    mXuiManager.setLocOnOff();
                    btnLoud.switchAnimation();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                break;
            //  重低音开关
            case R.id.btn_subWoof:
                try {
                    mXuiManager.setSubWoofOnOff();
                    btnSubWoof.switchAnimation();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                break;
            default:
                break;
        }
    }
}
