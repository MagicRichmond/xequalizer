package com.xui.xequalizer.adapter;

import android.content.Context;
import android.logic.XuiManager;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xui.xequalizer.R;
import com.xui.xequalizer.XEqualizerApp;
import com.xui.xequalizer.utils.SharePreUtils;
import com.xui.xequalizer.view.CustomCardView;


public class EffectAdapter extends BaseAdapter {

    private XuiManager mXuiManager = XEqualizerApp.getXuiManager();

    private Context mContext;
    //  选中id号
    private int mPosition;
    // 选中和未选中的图片背景
    private int mSelectedImages[];
    private int mUnselectedImages[];

    private int selectItem;

    private String[] cardText = {"爵士", "摇滚", "自定义", "流行", "古典"};

    private CustomCardView customCardView;

    public EffectAdapter(Context context, int[] selectedImages, int[] unselectedImages) {
        mContext = context;
        mSelectedImages = selectedImages;
        mUnselectedImages = unselectedImages;
    }

    @Override
    public int getCount() {
        return cardText.length * 2;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        customCardView = new CustomCardView(mContext);

        mPosition = position % mSelectedImages.length;

        TextView textView = (TextView) customCardView.findViewById(R.id.card_text);
        textView.setText(cardText[mPosition]);

        //  选中自定义时，按钮显示圆角框
        if (mPosition == 2) {
            customCardView.findViewById(R.id.center_btn_bg).setVisibility(View.VISIBLE);
        } else {
            customCardView.findViewById(R.id.center_btn_bg).setVisibility(View.GONE);
        }


        //  选中切换背景图
        if (position == selectItem) {
            if (mPosition == 2) {
                customCardView.findViewById(R.id.center_btn_bg).setSelected(true);
                try {
                    mXuiManager.setSoundInfo(SharePreUtils.getUVolume("initUVol", 0, XEqualizerApp.getContext()),
                            SharePreUtils.getMVolume("initMVol", 0, XEqualizerApp.getContext()),
                            SharePreUtils.getHVolume("initHVol", 0, XEqualizerApp.getContext()));
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }

            customCardView.findViewById(R.id.center_item).setBackgroundResource(mSelectedImages[mPosition]);
            customCardView.setBackgroundResource(R.drawable.circle_rectangle_selected);
        } else {
            customCardView.findViewById(R.id.center_item).setBackgroundResource(mUnselectedImages[mPosition]);
            customCardView.findViewById(R.id.card_bg).setBackgroundResource(R.drawable.circle_unselected_bg);
            customCardView.setBackgroundResource(R.drawable.circle_rectangle_unselected);
        }

        return customCardView;
    }

    /**
     * 设置选中频道
     * @param selectItem
     */
    public void setSelectItem(int selectItem) {

        if (this.selectItem != selectItem) {
            this.selectItem = selectItem;
            notifyDataSetChanged();
        }
    }

}
